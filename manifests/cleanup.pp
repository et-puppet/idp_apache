#
# Cleanup after install
#

class idp_apache::cleanup (
  $log_dir,
) {

  include stdlib

  unique(
    [
      $log_dir,
      '/var/log/apache2',
      '/var/lock/apache2',
      '/var/run/apache2',
    ]
  ).each | $dir | {
    exec { "clean-${dir}":
      command => "/bin/rm -rf ${dir}/*",
      cwd     => '/',
    }
  }
}
