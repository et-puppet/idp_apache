# Class: idp_apache
# ===========================
#
# Full description of class idp_apache here.
#
# Examples
# --------
#
# @example
#    class { 'idp_apache': }
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
# Vivien Wu <vivienwu@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2018 The Board of Trustees of the Leland Stanford Junior
# University
#
class idp_apache (
  $log_dir,
  $enabled,
  $ensure,
) {

  stage { 'cleanup': }

  Stage['main'] -> Stage['cleanup']

  # force an apt-get update before installing any packages
  include apt
  Exec['apt_update'] -> Package<| |>

  class { 'apache':
    service_enable      => $enabled,
    service_ensure      => $ensure,
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      vhost_common => '%v %h %l %u %t \"%r\" %>s %b',
      combined_elb => '%v %{c}a %a %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"'
    },
  }

  package { 'authbind':
    ensure => latest
  }

  package { 'lynx':
    ensure => latest
  }

  file {
    [
      '/etc/authbind/byport/80',
      '/etc/authbind/byport/443',
    ]:
      ensure  => file,
      owner   => 'www-data',
      group   => 'www-data',
      mode    => '0755',
      require => Package['authbind','httpd','lynx'],
  }

  apache::mod {
    [
      'env',
      'rewrite',
      'access_compat',
      'proxy',
      'proxy_balancer',
      'proxy_http',
      'remoteip',
    ]:
  }

  file {
    [
      '/var/log/apache2',
      '/var/lock/apache2',
      '/var/run/apache2',
    ]:

    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0755',
    require => Package['httpd'],
  }

  # ensure the private key directory is readable by the apache user
  file { '/etc/ssl/private':
    ensure  => directory,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0750',
    require => Package['httpd'],
  }

  file { '/etc/apache2/conf.d/platform_env.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/platform_env.conf",
  }

  file { '/etc/apache2/conf.d/05-rotatelogs.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/05-rotatelogs.conf",
  }

  file { '/start.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/start.sh",
  }

  class { 'idp_apache::cleanup':
    stage   => 'cleanup',
    log_dir => $log_dir,
  }

}
