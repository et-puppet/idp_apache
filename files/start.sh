#! /bin/bash

cmd="authbind --deep /usr/sbin/apache2ctl -D FOREGROUND -k start"

[ "$(id -un)" == "www-data" ] || cmd="su -s /bin/sh -c /start.sh www-data"

exec $cmd
